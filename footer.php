<footer>
  <div class="twelve columns">
  <ul class="social-links">
         <li><a href="https://www.facebook.com/alief85" target="_blank"><i class="fa fa-facebook"></i></a></li>
         <li><a href="https://twitter.com/aliefrahman" target="_blank"><i class="fa fa-twitter"></i></a></li>
         <li><a href="https://plus.google.com/u/0/111545063680462820275" target="_blank"><i class="fa fa-google-plus"></i></a></li>
         <li><a href="https://bitbucket.org/aliefrahman/" target="_blank"><i class="fa fa fa-bitbucket"></i></a></li>
         <li><a href="https://www.instagram.com/aliefrahman_mone/" target="_blank"><i class="fa fa-instagram"></i></a></li>
      </ul>
  </div>
   <div class="row">
     <div class="four columns">
    <p class="copyright">
     <?php
      if( get_theme_mod( 'artux_copyright' ) == '') { ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a> |
      <a href="<?php echo esc_url( 'http://www.aliefrahman.info/'); ?>"><?php printf( __( 'made indonesia', 'artux_theme' ), 'WordPress' ); ?></a>
      <?php }
      else { echo wp_kses_post( get_theme_mod( 'artux_copyright' ) ); } ?>
    </p>
  </div>
    <div class="eight columns">
      <p class="copyright2">
      <?php
       if( get_theme_mod( 'artux_copyright_2' ) == '') { ?>
       <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a> |
       <a href="<?php echo esc_url( 'http://www.aliefrahman.info/'); ?>"><?php printf( __( 'made indonesia', 'artux_theme' ), 'WordPress' ); ?></a>
       <?php }
       else { echo wp_kses_post( get_theme_mod( 'artux_copyright_2' ) ); } ?>
     </p>
    </div>
   </div> <!-- End row -->

   <div id="go-top"><a class="smoothscroll" title="Back to Top" href="#top"><i class="fa fa-chevron-up"></i></a></div>

</footer> <!-- End Footer-->

<!-- Java Script
================================================== -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script id="dsq-count-scr" src="//aliefrahman-info.disqus.com/count.js" async></script>
<script class="cssdeck" src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php bloginfo('template_directory');?>/js/jquery-1.10.2.min.js"><\/script>')</script>
</body>

</html>
