<?php get_header(); ?>

<!-- Content
================================================== -->
<div id="content-wrap">
 <div class="row">
	 <div id="main" class="eight columns">
		 <?php if (have_posts()) : ?>
                        <?php while (have_posts()) : the_post(); ?>
											<article class="entry">
												<header class="entry-header">
													<h2 class="entry-title">
                            <?php the_title(); ?>
													</h2>
													<div class="entry-meta">
														<ul>
															<li><?php the_time('j F Y ') ?> </li>
															<span class="meta-sep">&bull;</span>
															<li>Category : <?php the_category(', ') ?></li>
															<li><?php edit_post_link('<span class="meta-sep">&bull;</span> Edit', '', '  '); ?></li>
														</ul>
													</div>

												</header>
												<div class="entry-content">
                            <?php the_content(); ?>
												</div>
												<p class="tags"><?php the_tags(); ?></p>
												<ul class="post-nav group">
												<?php $postslist = get_posts('numberposts=5&order=DESC'); foreach ($postslist as $post) : setup_postdata($post); ?>
												<li class="prev"><a href="<?php the_permalink(); ?>" rel="prev"><strong><?php the_title(); ?></strong></a>
													<?php the_time('F j, Y') ?>
												</li>
												<?php endforeach; ?>
												</ul>
												<ol class="commentlist">

				                   <li class="depth-1">

				                      <div class="avatar">
				                         <?php echo get_avatar(get_the_author_email(), '100'); ?>
				                      </div>

				                      <div class="comment-content">

				 	                     <div class="comment-info">
				 	                        <cite><strong><?php the_author_posts_link(); ?></strong></cite>

				 	                        <div class="comment-meta">

																		<a href="<?php the_author_meta('facebook', $current_author->ID); ?>"><i class="fa fa-facebook-square fa-lg"></i></a>
																		<span class="sep">/</span>
																		 <a class="reply" href="<?php the_author_meta('twitter', $current_author->ID); ?>"><i class="fa fa-twitter-square fa-lg"></i></a>
																			<span class="sep">/</span>
																			<a class="reply" href="<?php the_author_meta('linkedin', $current_author->ID); ?>"><i class="fa fa-linkedin-square fa-lg"></i></a>
				 	                        </div>
				 	                     </div>
				 	                     <div class="comment-text">
																 <p><?php the_author_description(); ?></p>
																	</div>
				 	                  </div>
				                   </li>
												</ol>
											</article>
																<div id="disqus_thread"></div>
																<script>

																/**
																 *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
																 *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables */
																/*
																var disqus_config = function () {
																    this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
																    this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
																};
																*/
																(function() { // DON'T EDIT BELOW THIS LINE
																    var d = document, s = d.createElement('script');
																    s.src = '//aliefrahman-info.disqus.com/embed.js';
																    s.setAttribute('data-timestamp', +new Date());
																    (d.head || d.body).appendChild(s);
																})();
																</script>
																<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

                        <?php endwhile; wp_reset_query(); ?>

                        <?php else : ?>

                        <?php endif; ?>
											</div>
											</div>
                  </div>

<?php get_footer(); ?>
