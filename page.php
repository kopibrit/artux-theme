<?php get_header(); ?>
        <!-- content -->
        <div id="content-wrap">
         <div class="row">
           <div id="main" class="eight columns">
                        <?php if (have_posts()) : ?>
                        <?php while (have_posts()) : the_post(); ?>
                        <div class="post">
                            <div class="grid_8 alpha ">
                            <h2><?php the_title(); ?> <?php edit_post_link(' | Edit', '', '  '); ?></h2>
                            <p class="categ"><?php the_time('j M Y ') ?>  </p>

                            <?php the_content(); ?>

                            <div class="clear"></div>
                            <?php comments_template( '', true ); ?>
                            </div>


                        </div>

                        <?php endwhile; wp_reset_query(); ?>

                        <?php else : ?>

                        <?php endif; ?>

                    </div>
                    <div id="sidebar" class="four columns">
                        <?php get_sidebar(category); ?>
                      </div>
                </div>
            </div>
      
        <!-- content -->

        <?php get_footer(); ?>
