<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head();?>
</head>
<body>
   <header id="top">
   	<div class="row">
   		<div class=" header-content twelve columns">
        <h1 id="logo-text"><a href="<?php bloginfo('wpurl');?>"><img src="<?php bloginfo('template_directory');?>/images/logo.png"></a</h1>
			</div>
	  </div>
    <nav id="nav-wrap">
     <div class="row">
       <div class="eight columns">
             <?php
      if ( has_nav_menu( 'header' ) ) {
        // User has assigned menu to this location;
        // output it
        wp_nav_menu( array(
            'theme_location' => 'header',
            'menu_class' => 'nav',
            'menu_id' => 'nav',
            'container' => ''
        ) );
      }
      ?>
    </div>
    <div class="four columns">
      <div class="date">
        <div class="widget widget_search">
                  <form action="#">
                     <input type="text" value="Search here..." onblur="if(this.value == '') { this.value = 'Search here...'; }" onfocus="if (this.value == 'Search here...') { this.value = ''; }" class="text-search">
                     <input type="submit" value="Submit" class="submit-search">
                  </form>

      </div>
    </div>
    </div>
   </nav> <!-- end #nav-wrap -->

   </header> <!-- Header End -->
