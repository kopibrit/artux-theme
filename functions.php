<?php
/*********************************************************************************************
Styles & Scripts
*********************************************************************************************/

function artux_theme_scripts() {
    wp_enqueue_style( 'style-name', get_stylesheet_uri() );
		wp_enqueue_style( 'default', get_template_directory_uri() . '/css/default.css', false ,'3.0.0');
		wp_enqueue_style( 'layout', get_template_directory_uri() . '/css/layout.css', false ,'3.0.0');
		wp_enqueue_style( 'media-queries', get_template_directory_uri() . '/css/media-queries.css', false ,'3.0.0');
		wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/modernizr.js', array('jquery'), '', true );
    wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/main.js', array('jquery'), '', true );
}
add_action( 'wp_enqueue_scripts', 'artux_theme_scripts' );

/*********************************************************************************************
SUPPORT THUMBNAILS
*********************************************************************************************/
add_theme_support( 'post-thumbnails' );
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
        set_post_thumbnail_size( 9999, 9999 ); // default Post Thumbnail dimensions
}
if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'category-thumb', 300, 9999 ); //300 pixels wide (and unlimited height)
	add_image_size( 'blogindex-thumb', 600, 350, true ); //(cropped)
	add_image_size( 'sticky-thumb', 230, 150, true ); //(cropped)
}
/*********************************************************************************************
EXCERPTS
*********************************************************************************************/
function artux_get_custom_excerpt($count){
  global $post;
  $permalink = get_permalink($post->ID);
  $excerpt = get_the_content();
  $excerpt = strip_shortcodes($excerpt);
  $excerpt = strip_tags($excerpt);
  $excerpt = substr($excerpt, 0, $count);
  $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
  return $excerpt;
}


/*********************************************************************************************
SETUP, HEADER & FOOTER MENUS
*********************************************************************************************/
if ( ! isset( $content_width ) )
	$content_width = 640; /* pixels */

if ( ! function_exists( 'artux_setup' ) ) :
function artux_setup() {
	add_theme_support( 'title-tag' );
	add_theme_support( 'custom-background' );
	add_editor_style( 'custom-editor-style.css' );
	load_theme_textdomain( 'artux_theme', get_template_directory() . '/languages' );
	add_theme_support( 'automatic-feed-links' );
	register_nav_menus( array(
		'header' => __( 'Header Menu', 'artux_theme' ),
		'footer' => __( 'Footer Menu', 'artux_theme' ),
	) );
}
endif;
add_action( 'after_setup_theme', 'artux_setup' );

/*********************************************************************************************
CUSTOMIZER
*********************************************************************************************/
function artux_customize_register( $wp_customize ) {

	/*logo*/

	$wp_customize->add_section( 'artux_logo_section' ,
		array(
			'title'       => __( 'Logo', 'artux_theme' ),
			'priority'    => 30,
			'description' => __( 'Upload a logo for this theme', 'artux_theme'),
		));

	$wp_customize->add_setting( 'artux_logo' ,
		array (
			'sanitize_callback' => 'esc_url_raw',
		));

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'artux_logo',
		array(
			'label'    => __( 'Current logo', 'artux_theme' ),
			'section'  => 'artux_logo_section',
			'settings' => 'artux_logo',
		)));

	/*footer*/

	$wp_customize->add_section('artux_copyright_section',
		array(
			'title'       => __( 'Footer Copyright', 'artux_theme' ),
			'description' => __( '<b>Text Footer</b>'),
			'priority' => 35,
		));

		$wp_customize->add_setting('artux_copyright',
      array(
          'sanitize_callback' => 'artux_sanitize_text',
	    ));

	    $wp_customize->add_control('artux_copyright',
		  array(
			'label'    => __( 'Copyright text', 'artux_theme' ),
			'section' => 'artux_copyright_section',
			'type' => 'textarea',
		));

    $wp_customize->add_setting('artux_copyright_2',
      array(
          'sanitize_callback' => 'artux_sanitize_text',
	    ));

	    $wp_customize->add_control('artux_copyright_2',
		  array(
			'label'    => __( 'Copyright text 2', 'artux_theme' ),
			'section' => 'artux_copyright_section',
			'type' => 'textarea',
		));

}
add_action('customize_register', 'artux_customize_register');

/*********************************************************************************************
REGISTER WIDGETIZED AREAS
*********************************************************************************************/
function artux_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'sidebar-1', 'artux_theme' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="blog-widget widget %2$s">',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
		'after_widget'  => '</aside>',
	) );
	register_sidebar( array(
		'name'          => __( 'sidebar-2', 'artux_theme' ),
		'id'            => 'sidebar-2',
		'before_widget' => '<aside id="%1$s" class="blog-widget widget %2$s">',
		'before_title'  => '<h1 class="widget-title"><span class="htitle">',
		'after_title'   => '</span></h1>',
		'after_widget'  => '</aside>',
	) );
}
add_action( 'widgets_init', 'artux_widgets_init' );


function search_form_shortcode( )
{
    ob_start();

    get_search_form( );

    $html = ob_get_contents();
    ob_end_clean();

    return $html;
}

add_shortcode('search_form', 'search_form_shortcode');

function artux_sanitize_text( $input ) {
    return wp_kses_post( force_balance_tags( $input ) );
}

function getpagenavi(){
	?>
	<div id="navigation" class="clearfix">
	<?php if(function_exists('wp_pagenavi')) : ?>
	<?php wp_pagenavi() ?>
	<?php else : ?>
	        <div class="alignleft"><?php next_posts_link(__('&laquo; Older Entries','web2feeel')) ?></div>
	        <div class="alignright"><?php previous_posts_link(__('Newer Entries &raquo;','fabthemes')) ?></div>
	        <div class="clear"></div>
	<?php endif; ?>

	</div>

	<?php
	}

/* Days ago */
function days_ago() {
	$days = round((date('U') - get_the_time('U')) / (60*60*24));
	if ($days==0) {
		echo "Today";
	}
	elseif ($days==1) {
		echo "Yesterday";
	}
	else {
		echo "" . $days . " days ago";
	}
}
