<div class="grid_4 ">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-2') ) : ?>
				<div class="side widget">
						<div class="title-side">
								<h2>Categories</h2>
						</div>
						<ul>
								<?php wp_list_categories('hide_empty=0&orderby=id&title_li='); ?>
						</ul>
				</div>
						<div class="side">
						<div class="title-side">
								<h2>Tag Cloud</h2>
						</div>
						<?php wp_tag_cloud(); ?>
						</div>
				<?php endif; ?>

</div>
