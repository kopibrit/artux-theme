
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-1') ) : ?>
				<div class="widget">
				
					<div class="widget widget_categories group">
						<h3>Category</h3>
						<ul>
								<?php wp_list_categories('hide_empty=0&orderby=id&title_li='); ?>
						</ul>
					</div>
				</div>
				<select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
				  <option value=""><?php echo esc_attr( __( 'Select Month' ) ); ?></option>
				  <?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
				</select>

				<?php endif; ?>
