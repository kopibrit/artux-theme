<?php get_header(); ?>
        <!-- content -->

                    <div class="grid_8">
                        <?php
                        if(get_query_var('author_name')) :
                            $curauth = get_user_by('slug', get_query_var('author_name'));
                        else :
                            $curauth = get_userdata(get_query_var('author'));
                        endif;
                        ?>
                        <div class="post">
                            <h2>Hi! <?php echo $curauth->nickname; ?></h2>
                            <div class="author">
                                <div class="grid_2 alpha img">
                                    <?php echo get_avatar($curauth->user_email, '100'); ?>
                                </div>
                                <div class="grid_6 omega">
                                    <p><?php echo $curauth->user_description; ?></p>
                                    <dl>
                                        <dt>Website</dt>
                                        <dd><a href="<?php echo $curauth->user_url; ?>"><?php echo $curauth->user_url; ?></a></dd>
                                    </dl>
                                </div>
                            </div>
                            <h3>Posts by <?php echo $curauth->nickname; ?>:</h3>
                        </div>
                        <?php if (have_posts()) : ?>
                        <?php while (have_posts()) : the_post(); ?>
                        <div class="post">
                            <div class="grid_7 omega">
                            <h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a> <?php edit_post_link(' | Edit', '', '  '); ?></h2>
                            <p class="categ"><?php the_time('j M Y') ?> Post in : <?php the_category(', ') ?> | <?php comments_popup_link('0', '1', '% '); ?> comment</p>
                            <?php the_excerpt(); ?>
                            <div class="paku"></div>
                            </div>
                        </div>
                        <?php endwhile; wp_reset_query(); ?>
                        <div class="navigation">
                            <div class="navleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
                            <div class="navright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
                            <div class="clear"></div>
                        </div>
                        <?php else : ?>
                        <div class="post">
                            <p><?php _e('No posts by this author.'); ?></p>
                            </div>
                        <?php endif; ?>
                        
                    </div>
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
        <!-- content -->
        
        <?php get_footer(); ?>