<?php get_header(); ?>
        <!-- content -->
                    <div class="grid_8">
                        <?php if (have_posts()) : ?>
                        <?php while (have_posts()) : the_post(); ?>
                        <div class="post">
                            <div class="grid_8 omega">
                            <h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a> <?php edit_post_link(' | Edit', '', '  '); ?></h2>
                            <p class="categ"><?php the_time('j M Y ') ?> Post in : <?php the_category(', ') ?> | <?php comments_popup_link('0', '1', '% '); ?> comment</p>
                            <?php the_excerpt(); ?>
                            <div class="paku"></div>
                            </div>
                        </div>
                        <?php endwhile; wp_reset_query(); ?>
                        <div class="navigation">
                            <div class="navleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
                            <div class="navright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
                            <div class="clear"></div>
                        </div>
                        <?php else : ?>
                            <div class="post">
                            <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                            </div>
                        <?php endif; ?>
                        
                    </div>
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
        <!-- content -->
        
        <?php get_footer(); ?>