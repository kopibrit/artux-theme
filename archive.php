<?php get_header(); ?>

<!-- Content
================================================== -->
<div id="content-wrap">
 <div class="row">
	 <div id="main" class="eight columns">
		 <?php if (have_posts()) : ?>
                        <?php while (have_posts()) : the_post(); ?>
											<article class="entry">
												<header class="entry-header">
													<h2 class="entry-title">
                            <?php the_title(); ?>
													</h2>
													<div class="entry-meta">
														<ul>
															<li><?php the_time('j F Y ') ?> </li>
															<span class="meta-sep">&bull;</span>
															<li>Category : <?php the_category(', ') ?></li>
															<li><?php edit_post_link('<span class="meta-sep">&bull;</span> Edit', '', '  '); ?></li>
														</ul>
													</div>

												</header>
												<div class="entry-content">
                            <p><?php echo artux_get_custom_excerpt(190);?><a href="<?php the_permalink(); ?>" title="title"> [...]</a></p>
												</div>
											</article>
                        <?php endwhile; wp_reset_query(); ?>

                        <?php else : ?>

                        <?php endif; ?>
											</div>
                      <div id="sidebar" class="four columns">
                        <?php get_sidebar(); ?>
                      </div>
											</div>
                  </div>

<?php get_footer(); ?>
