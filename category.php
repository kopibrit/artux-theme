<?php get_header(); ?>
<!-- Content
================================================== -->
<div id="content-wrap">
 <div class="row">
   <div id="main" class="eight columns">

          <h2>Category : <?php the_category(', ') ?></h2><hr>
                        <?php if (have_posts()) : ?>
                        <?php while (have_posts()) : the_post(); ?>
                        <article class="entry">
                          <header class="entry-header">
                            <h2 class="entry-title">
                              <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                            </h2>
                            <div class="entry-meta">
                              <ul>
                            <li><?php the_time('j M Y') ?></li>
                            <span class="meta-sep">&bull;</span>
                              <li>Category : <?php the_category(', ') ?></li>
                              <li><?php edit_post_link('<span class="meta-sep">&bull;</span> Edit', '', '  '); ?></li>
                            </ul>
                            </div>
                            <div class="entry-content">
                            <p><?php echo artux_get_custom_excerpt(190);?><a href="<?php the_permalink(); ?>" title="title"> [...]</a></p>
                            </div>

                        </header>
                        <?php endwhile; wp_reset_query(); ?>
                        <div class="navigation">
                            <div class="navleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
                            <div class="navright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
                            <div class="clear"></div>
                        </div>
                        <?php else : ?>

                        <?php endif; ?>
                      </article>
                    </div>
                <div id="sidebar" class="four columns">
                    <?php get_sidebar(category); ?>
                  </div>
                </div>
            </div>

        <!-- content -->

        <?php get_footer(); ?>
