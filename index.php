<?php get_header(); ?>

<div id="content-wrap">
 <div class="row">
        <div id="main" class="eight columns">
          <article class="entry">
			<?php if ( have_posts() ) : ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
          <header class="entry-header">
          		<header class="entry-header">
          		<h2 class="entry-title">
              <a href="<?php the_permalink(); ?>" title="title"><?php the_title(); ?></a>
              </h2>
              <div class="entry-meta">
  							<ul>
  								<li><?php echo days_ago();?></li>
  								<span class="meta-sep">&bull;</span>
  								<li><?php _e('Category &#58;'); ?> <a href="#" title="" rel="category tag"><?php the_category(', ') ?></a></li>
  								<span class="meta-sep">&bull;</span>
  								<li><?php _e('by'); ?> <?php  the_author(); ?></li>
  							</ul>
  						</div>
              </header><!-- .entry-header -->
              <div class="entry-content">
    						<p><?php echo artux_get_custom_excerpt(190);?><a href="<?php the_permalink(); ?>" title="title"> [...]</a></p>
              </div>
				<?php endwhile; ?>
			<?php endif; ?>
    </article>
    <?php if ($paged > 1) { ?>

		<ul class="post-nav group">
			<li class="prev"><?php next_posts_link('<i class="fa fa-arrow-circle-o-left fa-2x"></i>'); ?></li>
			<li class="next"><?php previous_posts_link('<i class="fa fa-arrow-circle-o-right fa-2x"></i>'); ?></li>
		</ul>

		<?php } else { ?>
			<ul class="post-nav group">
        <li class="prev"><?php next_posts_link('<i class="fa fa-arrow-circle-o-left fa-2x"></i>'); ?></li>
      </ul>
		<?php } ?>

		<?php wp_reset_query(); ?>

    </div>
    <div id="sidebar" class="four columns">
      <?php get_sidebar(); ?>
    </div>
			</div><!-- #content -->
		</div><!-- #primary .site-content -->

<?php get_sidebar(footer); ?>
<?php get_footer(); ?>
